from influxdb import InfluxDBClient
import random
#------------------------------------------------
#----------------- 21-marzo-2024 ----------------
# funciona, crea datos y los guarda en influxDB
#------------------------------------------------

msg_data = [
{
	"measurement": "device2",
	"fields": {
			"temperatura": 12.1,
            "humedad":1.9
        },
        "tags": {
            "location":"Edificio 42",
            "time_zone": -3
        }
           #timestamp: new Date()
}
]

client = InfluxDBClient('localhost', 8086, 'root', 'root', 'datos')

client.create_database('datos')

client.write_points(msg_data)

result = client.query('select temperatura from device2;')

print("Result: {0}".format(result))
